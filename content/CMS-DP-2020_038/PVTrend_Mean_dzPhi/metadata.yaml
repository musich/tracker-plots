title: 'Average impact parameter in the longitudinal plane as a function of processed luminosity'
caption: 'The mean of the average impact parameter in the longitudinal plane $d_z$ as a function of processed luminosity. Only tracks with transverse momentum $p_T >$ 3 GeV are considered. The vertical black lines indicate the first processed run for 2016, 2017 and 2018 data-taking period, respectively. The vertical dotted lines indicate a change in the pixel tracker calibration. The blue points correspond to the results with the alignment constants used during data-taking, the red points to the 2016, 2017 and 2018 End-Of-Year (EOY) re-reconstruction (notice there is no EOY re-reconstruction for the last 33 $fb^{-1}$ of data-taking in 2018), and the green points to the results obtained in the Run-2 Legacy alignment procedure. During the LHC shutdown in winter 2016/17, the inner component (pixel detector) of the CMS tracking detector was replaced. The first few inverse picobarns of the 2017 pp collision run have been devoted to the commissioning of the new detector, resulting in sub-optimal tracking performance. This is visible in degraded impact parameter bias around the 36 $fb^{-1}$ mark. Apart from this short period, aligning the tracker improves the mean of this distribution. Short IOVs with suboptimal configuration of the pixel local reconstruction (e.g. different high-voltage settings, inconsistent local reconstruction with alignment) can give rise to isolated peaks in the trends, especially during data taking. The suboptimal performance for the alignment during data taking at the beginning of 2016 is due to relative misalignment of the two half-barrels of the pixel detector along the z-direction. This was not corrected by the alignment in the prompt calibration loop (PCL), which was not active in that period.'

date: '2020-07-22'
tags:
- Run-2
- pp
- Collisions
- Alignment
- Time Evolution
